﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Safe
{
    public class SafeState
    {
        public int Rows { get; }
        public int Cols { get; }
        public GridValue[,] Grid { get; }

        public GridValue CurrentVert { get; private set; }

        public event Action<int, int> MoveMade;

        private readonly Random random = new Random();
        public SafeState(int rows, int cols)
        {
            Rows = rows;
            Cols = cols;
            Grid = new GridValue[rows, cols];
            CurrentVert = GridValue.Horizontal;
            AddRandom();
        }


        private IEnumerable<Position> EmptyPositions()
        {
            for (int r = 0; r < Rows; r++)
            {
                for (int c = 0; c < Cols; c++)
                {
                    if (Grid[r, c] == GridValue.Empty)
                    {
                        yield return new Position(r, c);
                    }
                }
            }
        }

        private void AddRandom()
        {
            List<Position> empty = new List<Position>(EmptyPositions());

            if (empty.Count == 0)
            {
                return;
            }
            for (int r = 0; r < Rows + 20; r++)
            {
                for (int c = 0; c < Cols + 20; c++)
                {
                    Position pos = empty[random.Next(empty.Count)];
                    Grid[pos.Row, pos.Col] = GridValue.Horizontal;
                    Position pos2 = empty[random.Next(empty.Count)];
                    Grid[pos2.Row, pos2.Col] = GridValue.Vertical;
                }
            }

        }
        private void Replace()
        {
            for (int r = 0; r < Rows; r++)
            {
                for (int c = 0; c < Cols; c++)
                {
                    CurrentVert = CurrentVert == GridValue.Horizontal ? GridValue.Vertical : GridValue.Horizontal;
                }
            }
            
                
        }
        public void MakeMove(int r, int c)
        {
            Grid[r, c] = CurrentVert;
            Replace();
        }

        /*private void Wintype(int r, int c)
        {
            if (Grid[r,c] == GridValue.Vertical) {
            };
        }*/
    }
}
