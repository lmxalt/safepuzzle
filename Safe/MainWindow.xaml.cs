﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Safe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Dictionary<GridValue, ImageSource> gridValToImage = new()
        {
            

            {GridValue.Empty, Images.Empty },
            {GridValue.Horizontal, Images.Horizontal},
            {GridValue.Vertical, Images.Vertical}
        };
        public int N = 7;
        private int rows, cols;
        private void Field()
        {
            rows = N;
            cols = N;
        }

        private readonly Image[,] gridImages;
        private SafeState safeState;

        public MainWindow()
        {
            InitializeComponent();
            Field();
            gridImages = SetupGrid();
            safeState = new SafeState(rows, cols);

            safeState.MoveMade += OnMoveMade;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Draw();
        }

        private Image[,] SetupGrid()
        {

            Image[,] images = new Image[rows, cols];
            SafeGrid.Rows = rows;
            SafeGrid.Columns = cols;

            for (int r = 0; r < rows; r++)
            {
                for (var c = 0; c < cols; c++)
                {
                    Image image = new Image
                    {
                        Source = Images.Empty
                    };

                    images[r, c] = image;
                    SafeGrid.Children.Add(image);
                }
            }
            return images;
        }


        private void Draw()
        {
            DrawGrid();
        }
        private void DrawGrid()
        {
            for (int r = 0; r < rows; r++)
            {
                for (var c = 0; c < cols; c++)
                {
                    GridValue gridVal = safeState.Grid[r, c];
                    gridImages[r, c].Source = gridValToImage[gridVal];
                }
            }
        }

        

        private void OnMoveMade(int r, int c)
        {
            MessageBox.Show("You click on the button");
            GridValue gridVal = safeState.Grid[r, c];
            gridImages[r, c].Source = gridValToImage[gridVal];
           
                        }
        private void Win(int r, int c)
        {
            bool win = false;
            if (gridImages[r, c].Source == Images.Horizontal)
            {
                win = true;
                MessageBox.Show("Победа");
            };
        }

        private void SafeGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            double squareSize = SafeGrid.Width / N;
            Point clickPosition = e.GetPosition(SafeGrid);
            int row = (int)(clickPosition.Y / squareSize);
            int col = (int)(clickPosition.X / squareSize);

            bool win = false;
           
                for (int r = 0; r < N; r++)
                {
                    for (int c = 0; c < N; c++)
                    {
                        if (r == row || c == col)
                        {

                            GridValue gridVal = safeState.Grid[r, c];
                            
                            if (gridImages[r, c].Source == Images.Vertical)
                            {
                                //MessageBox.Show("You click on the button 1");
                                //gridImages[r, c].Source = new BitmapImage(new Uri("pack://application:,,,/Assets/Horizontal.png"));
                                gridImages[r, c].Source = Images.Horizontal;
                            }
                            else
                            {
                            gridImages[r, c].Source = Images.Vertical;
                            }

                        }

                    }

                }
            
                 
            /*if (gridImages[rows, cols].Source == Images.Horizontal)
                {
                win = true;
                };
                
            if (win)
            {
                MessageBox.Show("Победа");
            }*/

        }
    }
}