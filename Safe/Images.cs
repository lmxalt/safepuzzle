﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Safe
{
    public static class Images
    {
        public readonly static ImageSource Empty = LoadImage("Empty.png");
        public readonly static ImageSource Horizontal = LoadImage("Horizontal.png");
        public readonly static ImageSource Vertical = LoadImage("Vertical.png");
        private static ImageSource LoadImage(string fileName)
        {
            return new BitmapImage(new Uri($"Assets/{fileName}", UriKind.Relative));
        }
    }
}
